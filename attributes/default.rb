# Default Node Attributes for a C-Pod
#
default[:cpod][:url]        = "http://#{node[:fqdn]}"
default[:cpod][:repodir]    = '/srv/c-pod'
default[:cpod][:datadir]    = '/srv/cpoddata'
default[:cpod][:owner_name] = 'c-pod'

# Default Node Attributes for the SOCKS server
# All platforms should be set to use eth0
# Disable 'predictable' network interfaces in CentOS 7
# (Ubuntu 14.10 does this).
#

default[:cpod][:docker][:interface]  = 'docker0'

default[:cpod][:socks][:public_ifs]   = %w[eth0]

# vim: ft=ruby sts=2 sw=2 ts=8
