# Setup Docker server
#
require 'open-uri'
include_recipe 'c-pod::base'

case node[:platform_family]
when 'rhel'

  case osver
  when 5...7
    package 'docker-io'
  when 7...8
    package 'iptables'
    package 'xz'
    package 'procps'
    package 'git'

    # Setup Docker from binaries
    # (based on: https://docs.docker.com/installation/binaries/)
    # To own the Unix socket
    #
    group 'docker' do
      system  true
    end

    # https://raw.githubusercontent.com/tianon/cgroupfs-mount/master/cgroupfs-mount
    #
    docker_version = open("https://get.docker.com/builds", &:read).slice(/docker-(\d+\.\d+\.\d+)/, 1)

    remote_file '/usr/bin/docker' do
      source "https://get.docker.com/builds/Linux/x86_64/docker-#{docker_version}"
      mode    0755
      owner   'root'
      group   'root'
      notifies :restart, "service[docker]", :delayed
    end
    cookbook_file '/lib/systemd/system/docker.service' do
      action  :create
      mode    0644
      owner   'root'
      group   'root'
      notifies :restart, "service[docker]", :delayed
    end
    cookbook_file '/lib/systemd/system/docker.socket' do
      action  :create
      mode    0644
      owner   'root'
      group   'root'
      notifies :restart, "service[docker]", :delayed
    end
  end
  cookbook_file '/etc/sysconfig/docker' do
    source  'docker.defaults'
    action  :create
    mode    0755
    owner   'root'
    group   'root'
    notifies :restart, "service[docker]", :delayed
  end

when 'debian'

  apt_repository 'docker' do
    uri          'https://get.docker.com/ubuntu'
    arch         'amd64'
    distribution 'docker'
    components   ['main']
    keyserver    'keyserver.ubuntu.com'
    key          '36A1D7869245C8950F966E92D8576A8BA88D21E9'
  end

  package 'lxc-docker'
  cookbook_file '/etc/default/docker' do
    source  'docker.defaults'
    action  :create
    mode    0755
    owner   'root'
    group   'root'
    notifies :restart, "service[docker]", :delayed
  end

else
  error "Not supported"

end

service 'docker' do
  supports :restart => true, :reload => true
  action [:enable, :start]
end

# vim: sts=2 sw=2 ts=8
