# A Recipe to setup a C-Pod as a VM host only
# This does not include the Kickstart, YUM and GEM repository functions
# Use recipe 'c-pod::repo_host' for that.
#
include_recipe 'c-pod::base'
include_recipe 'c-pod::user'
include_recipe 'c-pod::git_repo'
include_recipe 'c-pod::virt'
include_recipe 'c-pod::socks'

# vim: sts=4 sw=4 ts=8
