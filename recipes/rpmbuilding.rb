# A Recipe to setup RPM building goodies
#
yum_package 'yum-utils'
yum_package 'rpm-build'
yum_package 'epel-release'
yum_package 'rpmforge-release'

# vim: sts=4 sw=4 ts=8
