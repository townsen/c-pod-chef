# C-Pod client configuration
# Setup repositories, have Avahi running and advertize SSH
#
case node[:platform_family]
when 'rhel'

  yum_globalconfig '/etc/yum.conf' do
    exactarch       true
    multilib_policy 'best'
    timeout         '10'
    action          :create
  end

  execute "create-yum-cache" do
    command "yum -q makecache"
    action :nothing
  end

  ruby_block "reload-internal-yum-cache" do
    block do
      Chef::Provider::Package::Yum::YumCache.instance.reload
    end
    action :nothing
  end

  case osver
  when 5...6
    yum_package 'yum-priorities'
  when 6...8
    yum_package 'yum-plugin-priorities'
  end

  # Create the Yum repository configuration for the C-Pod server
  # from the server itself.
  # The primary definition of this is in the samples directory
  # served by the C-Pod server and used during Kickstart.
  # Keep things DRY: Don't put it in a template
  #
  remote_file '/etc/yum.repos.d/c-pod.repo' do
    action      :create
    source      "#{node[:cpod][:url]}/samples/c-pod.repo"
    notifies :run, "execute[create-yum-cache]", :immediately
    notifies :create, "ruby_block[reload-internal-yum-cache]", :immediately
  end

  package 'avahi'
  package 'nss-mdns'

when 'debian'
  include_recipe 'apt'

  package 'avahi-daemon'
  package 'libnss-mdns'

else
  error "Not supported"

end

cookbook_file '/etc/avahi/services/ssh.service' do
  action  :create
  mode    0755
  owner   'root'
  group   'root'
end

# vim: sts=2 sw=2 ts=8
