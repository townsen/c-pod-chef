# Recipe for a C-Pod Repository Host
# Just the repos, no Docker or KVM - see other recipes for those
#

case node[:platform_family]
when 'rhel'
    include_recipe 'yum'
    package 'avahi'
    package 'createrepo'
    package 'yum-utils'
    package 'bsdtar'
    package 'ruby'
    package 'ruby-devel'
    case osver
    when 5...7
        node.default['apache']['version'] = '2.2'
    when 7...8 
        node.default['apache']['version'] = '2.4'
    else
        error "CentOS #{osver} is not supported yet!"
    end

when 'debian'
    include_recipe 'apt'
    package 'avahi-daemon'
    package 'createrepo'
    package 'bsdtar'
    package 'yum-utils'
    case osver
    when 10...14
        node.default['apache']['version'] = '2.2'
        package 'ruby1.9.3'
        package 'ruby1.9.1-dev'
    when 14.04
        node.default['apache']['version'] = '2.4'
        package 'ruby1.9.3'
        package 'ruby1.9.1-dev'
    when 14.10
        node.default['apache']['version'] = '2.4'
        package 'ruby2.1'
        package 'ruby2.1-dev'
    else
        error "Ubuntu #{osver} is not supported yet!"
    end

when 'mac_os_x'
    error "Not supported yet!"
end

node.default['apache']['default_modules']  +=  %w{
    actions
    cgi
    include
}

node.default['apache']['default_site_enabled'] = false

include_recipe 'apache2::default'

web_app 'c-pod' do
    template	'_c-pod.conf.erb'
    enable	true
    notifies :restart, "service[apache2]", :delayed
end

include_recipe 'c-pod::user'

g = resources("group[#{node[:cpod][:owner_name]}]")
g.members node['apache']['user']
g.append true
g.notifies :restart, "service[apache2]", :immediate

include_recipe 'c-pod::devtools'

gem_package 'redcarpet'
gem_package 'builder'

include_recipe 'c-pod::git_repo'
include_recipe 'c-pod::repo_structure'
include_recipe 'c-pod::chef-solo'

# vim: sts=4 sw=4 ts=8
