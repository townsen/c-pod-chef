# A Recipe to setup Supervisor Daemon
#

case node[:platform_family]
when 'rhel', 'debian'

    group 'supervisor' do
        action :create
    end

    easy_install_package 'supervisor'

    directory '/etc/supervisord.d' do
        mode    0750
    end

    cookbook_file '/etc/supervisord.conf' do
        mode    0640
        owner   'root'
        group   'supervisor'
    end

    cookbook_file '/etc/sysconfig/supervisord' do
        source  'supervisord.defaults'
        mode    0640
        owner   'root'
        group   'root'
    end

    cookbook_file '/etc/init.d/supervisord' do
        source  'supervisord.initd'
        mode    0750
        owner   'root'
        group   'root'
    end

    service 'supervisord' do
        supports :restart => true, :reload => true
        action [:enable, :start]
    end

when 'mac_os_x'
    error "This is never likely to be supported!"

end

# vim: sts=4 sw=4 ts=8
