# Chef Cookbooks for C-Pod

A [C-Pod](https://github.com/townsen/c-pod) is configured using [Chef-Solo](https://downloads.chef.io).

This doesn't require a Chef Server and can be done locally.

Note that this repository is not used standalone, it is a submodule with the main C-Pod repository.

In order for the recipes to work, the submodule name should be 'c-pod'.
